package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/service/cloudwatch"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// Request Struct
type Request struct {
	Records []struct {
		SNS struct {
			Timestamp  string `json:"Timestamp"`
			SNSMessage string `json:"Message"`
		} `json:"Sns"`
	} `json:"Records"`
}

// SNSMessage Struct
type SNSMessage struct {
	AlarmName        string `json:"AlarmName"`
	AlarmDescription string `json:"AlarmDescription"`
	NewStateValue    string `json:"NewStateValue"`
	NewStateReason   string `json:"NewStateReason"`
	ImageURL         string `json:"ImageURL"`
}

// SlackMessage Struct
type SlackMessage struct {
	Text        string       `json:"text"`
	Attachments []Attachment `json:"attachments"`
	ImageURL    string
	ThumbURL    string
	UnfurlLinks bool
	Fallback    string
	Pretext     string
}

// Attachment Struct
type Attachment struct {
	Text        string `json:"text"`
	Color       string `json:"color"`
	Title       string `json:"title"`
	ImageURL    string
	ThumbURL    string
	UnfurlLinks bool
	Fallback    string
	Pretext     string
}

func handler(request Request) error {
	var snsMessage SNSMessage
	date := strconv.FormatInt(time.Now().Unix(), 10)
	bucket := "zoom-alert"
	host := fmt.Sprintf("https://%s.s3.amazonaws.com", bucket)
	image := fmt.Sprintf("alert-%s.png", date)

	// Open sessiong
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-east-1"),
	})
	if err != nil {
		log.Println("Error creating session.")
		return err
	}

	// New cloudwatch client
	cw := cloudwatch.New(sess)

	// Create image request
	req, out := cw.GetMetricWidgetImageRequest(&cloudwatch.GetMetricWidgetImageInput{
		MetricWidget: aws.String(`{
			"metrics": [
				[ { "expression": "m1-m1+(MAX(m1)+MIN(m1))/2", "label": "AVG", "id": "e1" } ],
				[ { "expression": "m1+2*STDDEV(m1)", "label": "STDDEV", "id": "e2", "visible": false } ],
				[ { "expression": "e2-e1", "label": "Rate (2STDDEV-AVG)", "id": "e3" } ],
				[ "AWS/ApplicationELB", "RequestCount", "LoadBalancer", "app/prod-zlead/b9629aeb7dc8ac7d", { "stat": "Sum", "id": "m1", "period": 60, "label": "RequestCount per minute" } ]
			],
			"view": "timeSeries",
			"stacked": false,
			"title": "Leads Alert",
			"period": 300,
			"width": 1286,
			"height": 220,
			"start": "-PT3H",
			"end": "P0D"
		}`),
	})

	// Send request and populate output
	err = req.Send()
	if err != nil {
		log.Println("Error getting image.")
		return err
	}

	// Upload to s3
	_, err = s3.New(sess).PutObject(&s3.PutObjectInput{
		Bucket:      aws.String(bucket),
		Key:         aws.String(image),
		Body:        bytes.NewReader(out.MetricWidgetImage),
		ACL:         aws.String("public-read"),
		ContentType: aws.String("image/png"),
	})
	if err != nil {
		log.Println("Error uploading to s3.")
		return err
	}

	// Build SNS Message
	err = json.Unmarshal([]byte(request.Records[0].SNS.SNSMessage), &snsMessage)
	if err != nil {
		log.Println("Error unmarsheling snsMessage.")
		return err
	}
	snsMessage.ImageURL = fmt.Sprintf("%s/%s", host, image)

	log.Printf("New alarm: %s - Reason: %s", snsMessage.AlarmName, snsMessage.NewStateReason)

	// Build Slack Message
	slackMessage := buildSlackMessage(snsMessage)

	// Post to Slack
	err = postToSlack(slackMessage)
	if err != nil {
		log.Println("Error posting to slack.")
		return err
	}

	log.Println("Notification has been sent")
	return nil
}

func buildSlackMessage(message SNSMessage) SlackMessage {
	log.Println(message.ImageURL)

	text := fmt.Sprintf("%s\n%s\n%s\n<%s|Veja> a foto para mais detalhes!",
		message.AlarmName, message.AlarmDescription, message.NewStateReason, message.ImageURL)
		
	return SlackMessage{
		Text: message.ImageURL,
		Attachments: []Attachment{
			Attachment{
				Text:        text,
				Color:       "danger",
				Title:       "QUEDA DE VENDA",
				ImageURL:    message.ImageURL,
				UnfurlLinks: true,
				Fallback:    text,
				Pretext:     message.ImageURL,
			},
		},
	}
}

func checkError(f func() error) {
	if err := f(); err != nil {
		log.Println("Received error:", err)
	}
}

func postToSlack(message SlackMessage) error {
	client := &http.Client{}
	data, err := json.Marshal(message)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", os.Getenv("SLACK_WEBHOOK"), bytes.NewBuffer(data))
	if err != nil {
		return err
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer checkError(resp.Body.Close)

	if resp.StatusCode != 200 {
		fmt.Println(resp.StatusCode)
		return err
	}

	return nil
}

func main() {
	lambda.Start(handler)
}
